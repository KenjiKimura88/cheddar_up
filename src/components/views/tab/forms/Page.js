
import React from 'react'
import { Panel } from 'elements'
import tx from 'theme/utilities'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import { tabsPathHelper } from 'helpers'
import { DashboardLayout } from 'layout'
import { compose, pure } from 'recompose'
import { Nav as FormsNav } from 'views/tab/forms'
import { StyleSheet, css } from 'aphrodite/no-important'
import { Nav as TabNav } from 'views/tab'

const sx = StyleSheet.create({
  addFormIcon: {
    height: 75
  },
  panel: {
    width: '100%',
    maxWidth: 700
  }
})

const enhance = compose(
  connect(
    ({
      tabs: { tab }
    }) => ({
      tab
    })
  ),
  pure
)

export default enhance(({ tab }) =>
  <DashboardLayout
    primarySidebar={{ tab, nav: <TabNav /> }}
    secondaryNavbar={{ tab }}
    secondarySidebar={{ nav: <FormsNav /> }}>
    <div className={css(tx.p2, tx.flex, tx.justifyCenter)}>
      <div className={css(sx.panel)}>
        <Link to={tabsPathHelper(tab, 'forms/add-form')}>
          <Panel>
            <div className={css(tx.p2, tx.textAlignCenter, tx.color_black)}>
              <h2 className={css(tx.pb2)}>
                Need to collect information?
                <br />
                Add forms to your tab:
              </h2>
              <img
                className={css(sx.addFormIcon)}
                src={require('theme/images/AddForm.svg')}
                role='presentation' />
            </div>
          </Panel>
        </Link>
      </div>
    </div>
  </DashboardLayout>
)
