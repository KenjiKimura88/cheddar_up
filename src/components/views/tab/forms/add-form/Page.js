
import React from 'react'
import Form from './Form'
import { connect } from 'react-redux'
import { PanelOverlayLayout } from 'layout'
import { Nav as ItemsNav } from 'views/tab/items'
import { Nav as TabNav } from 'views/tab'

const enhance = connect(
  ({
    tabs: { tab }
    // async: { statuses }
  }) => ({
    tab
    // status: statuses[CREATE_ITEM]
  }),
  {
    // createItem
  }
)

export default enhance(({ tab }) =>
  <PanelOverlayLayout
    primarySidebar={{ tab, nav: <TabNav /> }}
    secondaryNavbar={{ tab }}
    secondarySidebar={{ nav: <ItemsNav /> }}>
    <Form onSubmit={() => 'noop'} />
  </PanelOverlayLayout>
)
